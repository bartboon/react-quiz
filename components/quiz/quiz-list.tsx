import Link from "next/link";
import Image from "next/image";
import Card from "../UI/card";
import classes from "./question-list.module.css";

interface QuizListProps {
  quizes: Quiz[];
}

const QuizList: React.FC<QuizListProps> = ({ quizes }) => {
  return (
    <ul className={classes.list}>
      {quizes.map((quiz) => {
        const { id, title, image, description } = quiz;
        const quizLink = `/quiz/${id}`
        return (
          <Card key={quiz.id}>
            <section className={classes.question}>
              <Link href={quizLink}>
                <a>
                  <h3>
                    {id} {title}
                  </h3>
                  {image && (
                    <div className={classes.image}>
                      <Image
                        src={`/images/${image}`}
                        alt={image}
                        width={500}
                        height={300}
                      />
                    </div>
                  )}
                </a>
              </Link>
              <h4>{description}</h4>
            </section>
          </Card>
        );
      })}
    </ul>
  );
};

export default QuizList;
