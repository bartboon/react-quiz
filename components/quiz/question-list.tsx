import classes from "./question-list.module.css";
import Question from "./question";
import Card from "../UI/card";

interface QuestionListProps {
  questions: Question[];
  handleAnswerForQuestion: (question: QuestionAnswer) => void;
  feedback: boolean | undefined;
}

const QuestionList: React.FC<QuestionListProps> = (props) => {
  const { questions, handleAnswerForQuestion, feedback } = props;

  return (
    <ul className={classes.list}>
      {questions.map((question: Question) => {
        return (
          <Card key={question.id}>
            <Question
              question={question}
              onHandleAnswer={handleAnswerForQuestion}
              feedback={feedback}
            />
          </Card>
        );
      })}
    </ul>
  );
};

export default QuestionList;
