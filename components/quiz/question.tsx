import { ChangeEvent } from "react";
import Image from "next/image";
import classes from "./question.module.css";


interface QuestionProps {
  onHandleAnswer: (question: QuestionAnswer) => void;
  feedback: boolean | undefined;
  question: Question
}

const Question: React.FC<QuestionProps> = ({
  question,
  onHandleAnswer,
  feedback
}) => {
  const { id, title, category, image, text, answers } = question;
  // catch the event of the radio with the complete answer, then push into callback of questionList
  function radioChanged(event: ChangeEvent<HTMLInputElement>) {
    onHandleAnswer(JSON.parse(event.target.value));
  }
  return (
    <section className={classes.question}>
      <h3>
        {id} {title}
      </h3>
      { image &&
        (<div className={classes.image}>
          <Image src={`/images/${category}/${image}`} alt={image} width={500} height={300} />
        </div>)
      } 
      <h4>{text}</h4>
      <ol>
        {answers.map((answer, index) => (
          <li key={index}>
            <label>
              <input
                type="radio"
                name={`group_${id}`}
                value={JSON.stringify({ id, answer })}
                onChange={radioChanged}
              />
              <span>{answer.text}  { feedback && answer.correct && '                   V' }</span>
              
            </label>
          </li>
        ))}
      </ol>
    </section>
  );
};

export default Question;
