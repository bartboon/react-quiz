import React, { useState } from "react";
import classes from "./quiz.module.css";
import { QuizStatus } from '../../common';

interface QuizStatusProps {
  status: QuizStatus,
  numberCorrect? : number
  numberTotal? : number
}

const QuizStatusMessage: React.FC<QuizStatusProps> = ({ status, numberCorrect, numberTotal }) => {
    return (
      status === QuizStatus.Success && <p className={classes.success}>The entire quiz was successfull ! </p> ||
      status === QuizStatus.Incomplete && <p className={classes.incomplete}>Not everything was filled in</p> ||
      status === QuizStatus.Error &&  <p className={classes.error}>Quiz not entirely correct {numberCorrect} out of {numberTotal} correct</p> ||
      <p></p>
    );  
};

export default QuizStatusMessage;
