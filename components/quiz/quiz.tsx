import React, { useState } from "react";
import classes from "./quiz.module.css";
import QuestionList from "./question-list";
import { QuizStatus } from '../../common';
import QuizStatusMessage from "./quiz-status";

interface QuizProps {
  quiz: Quiz,
  questions: Question[];
}

const Quiz: React.FC<QuizProps> = ({ quiz, questions }) => {
  const [questionAnswers, setQuestionAnswers] = useState<QuestionAnswer[]>(
    questions.map((q) => {
      return { question: q, correct: undefined };
    })
  );
  const [quizStatus, setQuizStatus] = useState<QuizStatus | undefined>();
  const [showFeedback, setShowFeedback] = useState(false);
  const [numberCorrect, setNumberCorrect] = useState(0);
  const [numberTotal, setNumberTotal] = useState(0);

  function handleAnswerForQuestion(questionAnswer: any) {
    const newAnswerArray: QuestionAnswer[] = [...questionAnswers];
    const index = newAnswerArray.findIndex(
      (qa: QuestionAnswer) => qa.question.id === questionAnswer.id
    );
    newAnswerArray[index].correct = questionAnswer.answer.correct;

    setQuestionAnswers(newAnswerArray);
  }

  function handleCheckQuiz(event: any) {
    event.preventDefault();
    if (
      questionAnswers.some((qa: QuestionAnswer) => qa.correct === undefined)
    ) {
      setQuizStatus(QuizStatus.Incomplete);
      return;
    }
    if (questionAnswers.some((qa: QuestionAnswer) => qa.correct === false)) {
      setNumberTotal(questionAnswers.length);
      setNumberCorrect(
        questionAnswers.filter((q) => q.correct === true).length
      );
      setQuizStatus(QuizStatus.Error);
      setShowFeedback(true);
      return;
    }
    setQuizStatus(QuizStatus.Success);
  }

  return (
    <section>
      <h1 className={classes.title}>Quiz {quiz.title}</h1>
      <form className={classes.form}>
        <QuestionList
          questions={questions}
          handleAnswerForQuestion={handleAnswerForQuestion}
          feedback={showFeedback}
        />
        {quizStatus && <QuizStatusMessage status={quizStatus} numberCorrect={numberCorrect} numberTotal={numberTotal}></QuizStatusMessage>}
        <div className={classes.actions}>
          <button onClick={handleCheckQuiz}>Check my answers</button>
        </div>
      </form>
    </section>
  );
};

export default Quiz;
