import { useState, useRef } from 'react';
import { signIn, SignInResponse } from 'next-auth/client';
import { useRouter } from 'next/router';
import classes from './auth-form.module.css';


const AuthForm: React.FC = () => {

  const [isLogin, setIsLogin] = useState(true);

  const emailInputRef = useRef<HTMLInputElement>(null);
  const passwordInputRef = useRef<HTMLInputElement>(null);
  const router = useRouter();

  function switchAuthModeHandler() {
    setIsLogin((prevState) => !prevState);
  }

  async function createUser(email: string, password: string) {
    const response = await fetch('/api/auth/signup', {
      method: 'POST',
      body: JSON.stringify({ email, password}),
      headers: {
        'Content-type' : 'application/json'
      }
    });

    const data = await response.json();
    if(!response.ok) {
      throw new Error('could not signup ');
    }

  }

  async function submitHandler(event: { preventDefault: () => void; }) {
    event.preventDefault();
    const enteredPassword = passwordInputRef.current?.value as string;
    const enteredEmail = emailInputRef.current?.value as string;
    event.preventDefault();
    if(isLogin) {
      // set redirect to false when authentication fails, not an error page, show overlay or something 
      const result: SignInResponse | undefined = await signIn('credentials', { 
        redirect: false,
        email: enteredEmail,
        password: enteredPassword
      });
      
      if(result && !result.error) {
        router.replace('/quiz');    //navigate away after login, need SPA navigation, keep state NO window.location ... 
      }
    } else {
      try {
        createUser(enteredEmail, enteredPassword);
      } catch(error) {
        console.log(error);   // make notification bar for this later
      }  
    }
  } 

  return (
    <section className={classes.auth}>
      <h1>{isLogin ? 'Login' : 'Sign Up'}</h1>
      <form onSubmit={submitHandler}>
        <div className={classes.control}>
          <label htmlFor='email'>Your Email</label>
          <input ref={emailInputRef} type='email' id='email' required />
        </div>
        <div className={classes.control}>
          <label htmlFor='password'>Your Password</label>
          <input ref={passwordInputRef} type='password' id='password' required />
        </div>
        <div className={classes.actions}>
          <button>{isLogin ? 'Login' : 'Create Account'}</button>
          <button
            type='button'
            className={classes.toggle}
            onClick={switchAuthModeHandler}
          >
            {isLogin ? 'Create new account' : 'Login with existing account'}
          </button>
        </div>
      </form>
    </section>
  );
}

export default AuthForm;
