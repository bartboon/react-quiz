import { Fragment } from 'react';

import MainNavigation from './main-navigation';

interface LayoutProps {
  children: React.ReactNode
}

const Layout:React.FunctionComponent<LayoutProps> = (props) => {
  return (
    <Fragment>
      <MainNavigation />
      <main>{props.children}</main>
    </Fragment>
  );
}

export default Layout;