import Link from "next/link";
import { useSession, signout } from "next-auth/client";

import classes from "./main-navigation.module.css";

function MainNavigation() {
  const [session, loading] = useSession(); // gives back if we are still loading or we have a valid session

  function logoutHandler() {
    signout();
  }

  return (
    <header className={classes.header}>
      <Link href="/">
        <a>
          <div className={classes.logo}>Quizzy</div>
        </a>
      </Link>
      <li>
        <Link href="/quiz/1">Demo</Link>
      </li>
      {/* {session && ( */}
      <li>
        <Link href="/quiz">Our latest quizes</Link>
      </li>
      {/* )} */}
      <nav>
        <ul>
          {!session && !loading && (
            <li>
              <Link href="/auth">Login</Link>
            </li>
          )}

          {session && (
            <li>
              <button onClick={logoutHandler}>Logout</button>
            </li>
          )}
        </ul>
      </nav>
    </header>
  );
}

export default MainNavigation;
