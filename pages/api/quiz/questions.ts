import { NextApiRequest, NextApiResponse } from "next";

function handler(req: NextApiRequest, res: NextApiResponse) {
  res.status(200).json([
    {
      id: "1",
      category: "sealife",
      type: "mc",
      title: "The blue whale is the largest animal on earth",
      image: "blue_whale.jpeg",
      text: "What kind of animal is the blue whale",
      hint: "This animal has lungs",
      answers: [
        {
          text: "a mammel",
          correct: true,
        },
        {
          text: "a very big fish",
          correct: false,
        },
      ],
    },
    {
      id: "2",
      category: "sealife",
      type: "mc",
      title: "Whales",
      image: "humpback.jpeg",
      text: "How much do adult humpback whales weigh",
      hint: "This animal has lungs",
      answers: [
        {
          text: "10-15 tons",
          correct: false,
        },
        {
          text: "25-30 tons",
          correct: true
        },
        {
          text: ">35 tons",
          correct: false
        },

      ],
    },

    {
      id: "101",
      category: "animals",
      type: "mc",
      title: "A cat is a domestic animal",
      image: "cat.jpeg",
      hint: "The sound is high pitched",
      text: "What kind of sound does a cat make",
      answers: [
        {
          text: "Miauw",
          correct: true,
        },
        {
          text: "Bark",
          correct: false,
        },
      ],
    },
    {
      id: "102",
      category: "animals",
      type: "mc",
      title: "Zebra's live in Africa",
      image: "zebra.jpeg",
      hint: "I can already see them on the picture",
      text: "How many stripes hase a zebra",
      answers: [
        {
          text: "None",
          correct: false,
        },
        {
          text: "Five",
          correct: false,
        },
        {
          text: "Many",
          correct: true,
        },
      ],
    },
    {
      id: "103",
      category: "animals",
      type: "mc",
      title: "Elephans eat lots of food",
      image: "elephants.jpeg",
      hint: "Hint they are found around trees",
      text: "What is their favourite snack",
      answers: [
        {
          text: "Hay",
          correct: false,
        },
        {
          text: "Peanuts",
          correct: false,
        },
        {
          text: "Treebark",
          correct: true,
        },
        {
          text: "Apples",
          correct: false,
        },
      ],
    },
    {
      id: "300",
      category: "math",
      type: "mc",
      title: "Additions",
      text: "What is 999 + 999",
      answers: [
        {
          text: "1998",
          correct: true,
        },
        {
          text: "1999",
          correct: false,
        },
        {
          text: "1988",
          correct: false,
        },
        {
          text: "Non of the above",
          correct: false,
        },
      ],
    },

  
    {
      id: "201",
      category: "sint",
      type: "mc",
      title: "Pietenpraat",
      image: "rudolf.png",
      text: "Sinterklaas en Santa Claus zijn eigenlijk dezelfde persoon",
      answers: [
        {
          text: "Jazeker",
          correct: true,
        },
        {
          text: "Ammenooit niet, ik leef toch niet op de Noordpool",
          correct: false,
        },
        {
          text: "Hoezo dan? Heeft Amerigo zich als Rudolf vermomd?",
          correct: false,
        },
      ],
    },
    {
      id: "202",
      category: "sint",
      type: "mc",
      title: "Pietenpraat",
      image: "malle_piet.png",
      text: "Hoe heet deze piet",
      answers: [
        {
          text: "Zwakzinnige piet",
          correct: true,
        },
        {
          text: "Hoofdpiet",
          correct: false,
        },
        {
          text: "Pieterbaas",
          correct: false,
        },
        {
          text: "Malle pietje",
          correct: true,
        },
      ],
    },

    {
      id: "203",
      category: "sint",
      type: "mc",
      title: "Sint weetjes",
      image: "mijter.png",
      text: "Hoe heet dit hoofddeksel",
      answers: [
        {
          text: "Smijter",
          correct: false,
        },
        {
          text: "Lange puntmuts",
          correct: false,
        },
        {
          text: "Hoed",
          correct: false,
        },
        {
          text: "Mijter",
          correct: true,
        },
      ],
    },

    {
      id: "204",
      category: "sint",
      type: "mc",
      title: "Het journaal",
      image: "diewertje.png",
      text: "Wie presenteert het Sinterklaas journaal",
      answers: [
        {
          text: "Diewertje Slok op",
          correct: false,
        },
        {
          text: "Duwertje Blok",
          correct: false,
        },
        {
          text: "Diewertje Schrok",
          correct: false,
        },
        {
          text: "Diewertje Blok",
          correct: true,
        },
      ],
    },

    {
      id: "205",
      category: "sint",
      type: "mc",
      title: "Hoe onthoud ie het allemaal",
      image: "sint_boek.png",
      text: "Waar bewaart de Sint alle informatie die hij van de pieten over jullie krijgt",
      answers: [
        {
          text: "Op de google server",
          correct: false,
        },
        {
          text: "Daar heeft ie een mobiele app voor",
          correct: false,
        },
        {
          text: "Het grote boek",
          correct: true,
        },
        {
          text: "Zijn kleine notitie blokje",
          correct: false,
        },
      ],
    },


    {
      id: "206",
      category: "sint",
      type: "mc",
      title: "Etenswaar ",
      image: "kruidnoten.png",
      text: "Wat zijn dit nu eigenlijk helemaal",
      answers: [
        {
          text: "Pepernoten",
          correct: false,
        },
        {
          text: "Kruidnoten",
          correct: true,
        },
        {
          text: "Paranoten",
          correct: false,
        },
        {
          text: "Kleine kloten",
          correct: false,
        },
      ],
    },
    {
      id: "207",
      category: "sint",
      type: "mc",
      title: "Logistieke zaken",
      image: "stoomboot.png",
      text: "Wanneer kwam de stoomboot voor het eerst in Nederland aan",
      answers: [
        {
          text: "1820",
          correct: false,
        },
        {
          text: "1850",
          correct: true,
        },
        {
          text: "1902",
          correct: false,
        },
        {
          text: "Stoomboot? Sint was millieubewust dus een zeilboot",
          correct: false,
        },
      ],
    },
    {
      id: "208",
      category: "sint",
      type: "mc",
      title: "Sint gaat failliet",
      image: "geld.png",
      text: "Sint heeft diepe zakken, hoeveel gaf hij in 2016 aan kado's uit?",
      answers: [
        {
          text: "500 miljoen",
          correct: false,
        },
        {
          text: "800 miljoen",
          correct: false,
        },
        {
          text: "900 miljoen",
          correct: true,
        },
        {
          text: "1,2 miljard",
          correct: false,
        },
      ],
    },
    {
      id: "209",
      category: "sint",
      type: "mc",
      title: "Dierenzaken",
      image: "appels.png",
      text: "Wat eet het paard van de sint het liefst?",
      answers: [
        {
          text: "Hooi",
          correct: false,
        },
        {
          text: "Appels",
          correct: false,
        },
        {
          text: "Wortels",
          correct: true,
        },
        {
          text: "Speculaas",
          correct: false,
        },
      ],
    },
    {
      id: "210",
      category: "sint",
      type: "mc",
      title: "Paard van de sint",
      image: "amerigo.png",
      text: "Sinds welk jaar wordt het paard van Sinterklaas Amerigo genoemd",
      answers: [
        {
          text: "1980",
          correct: false,
        },
        {
          text: "1990",
          correct: true,
        },
        {
          text: "1992",
          correct: false,
        },
        {
          text: "Was altijd al Amerigo sukkel",
          correct: false,
        },
      ],
    },


  ]);
}

export default handler;
