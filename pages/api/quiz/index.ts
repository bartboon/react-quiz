import { NextApiRequest, NextApiResponse } from "next";

function handler(req: NextApiRequest, res: NextApiResponse) {
  res.status(200).json([
    {
      id: 1,
      title: "a quiz about ocean animals",
      description: "Quiz about living creatures in the ocean",
      categories: ["sealife"],
      level: "medium",
      image: "sealife/blue_whale.jpeg",
      feedback: [
        { successType: "percentage" },
        {
          success: "Yeah you have successfully passed the quiz about the ocean",
        },
        { failure: "Sorry you have to learn more about ocean animals" },
      ],
      threshold: 80,
    },
    {
      id: 2,
      title: "A quiz about animals living on the Serengethi",
      description:
        "Quiz about living creatures in the the heat of Africa. Questions about the big 5.",
      categories: ["animals"],
      level: "hard",
      image: "animals/elephants.jpeg",
    },
    {
      id: 3,
      title: "Sinterklaas quiz",
      description: "De 2021 sinterklaas quiz",
      categories: ["sint"],
      level: "hard",
      image: "sint/amerigo.png",
    },
  ]);
}

export default handler;
