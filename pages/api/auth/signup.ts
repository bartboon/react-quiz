import { NextApiRequest, NextApiResponse } from "next";
import { hashPassword } from "../../../lib/auth";
import { connectDatabase } from "../../../lib/db";

async function handler(req: NextApiRequest, res: NextApiResponse) {
  // signup route, validate and store user
  if (req.method === "POST") {
    const data = req.body;
    const { email, password } = data;

    if (
      !email ||
      !email.includes("@") ||
      !password ||
      password.trim().length < 7
    ) {
      res.status(422).json({
        message: "invalid password should be at least 7 characters long",
      });
      return;
    }

    const client = await connectDatabase();
    const db = client.db();
    
    const existingUser = await db.collection("users").findOne({ email: email });
    if (existingUser) {
      res.status(422).json({ message: "email already registered" });
      client.close();
      return;
    }

    // NO plain passwords are allowed (HASH)
    const hashedPw = await hashPassword(password);
    const result = await db.collection("users").insertOne({
      email: email,
      password: hashedPw,
    });
  } else {
    res.status(422).json({
      message: "http method must be POST",
    });
    return;
  }
}

export default handler;
