import NextAuth from "next-auth";
import Providers from "next-auth/providers";
import { verifyPassword } from "../../../lib/auth";
import { connectDatabase } from "../../../lib/db";

// we need [...nextauth], all special routes are handled by the next auth 
// export the function NextAuth and configure it, the returned value is what is enclosed in the token

export default NextAuth({
  // set jwt as session mechanism
  session: {
    jwt: true
  },
  providers: [
    Providers.Credentials({
      async authorize(credentials: { email: string, password: string}) {
        
        const client = await connectDatabase();
        const usersCollection = client.db().collection('users');
        const user = await usersCollection.findOne({email: credentials.email});
        if(!user) {
          throw new Error('no user found');
        }

        const valid = await verifyPassword(credentials.password, user.password);
        if(!valid) {
          throw new Error('invalid password');
        }
        client.close();
        
        // if we return an object we say to authorize that it was succeeded, will be wrapped in a json web token
        //console.log('signin success full for user ' + user.email);
        return { email: user.email };
      }
    })
  ]
});