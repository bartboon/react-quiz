import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { getSession } from 'next-auth/client';
import AuthForm from '../components/auth/auth-form';

const AuthPage: React.FC= () => {
  const router = useRouter();
  const [isLoading, setIsLoading] = useState(true);
  const LOADING_MESSAGE = 'loading ....';
  // navigate away from the auth page (login/signup) when already logged in 
  // do not use window.location.href, you will lose state
  useEffect(()=> {
    getSession().then(session => {
      if (session) {
        router.replace('/')    
      } else {
        setIsLoading(false);
      }
    })
  }, [router]);

  if(isLoading) {
    return <p>{LOADING_MESSAGE}</p>
  }
  return <AuthForm/>;
}

export default AuthPage;
