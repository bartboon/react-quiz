import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import StartingPageContent from '../components/starting-page/starting-page';

function HomePage() {
  return <StartingPageContent />;
}

export default HomePage;
