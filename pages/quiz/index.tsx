import { GetStaticProps } from "next";
import { getAllQuizes } from "../../lib/api-util";
import QuizList from "../../components/quiz/quiz-list";

interface QuizPageProps {
  quizes: Quiz[];
}

const QuizPage: React.FC<QuizPageProps> = (props) => {
  return <QuizList quizes={props.quizes}></QuizList>;
};

export default QuizPage;

export const getStaticProps: GetStaticProps = async () => {
  const quizes = await getAllQuizes();
  return {
    props: {
      quizes,
    },
    revalidate: 60,
  };
}
