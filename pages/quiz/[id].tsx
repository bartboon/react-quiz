import { GetStaticProps, GetStaticPaths } from "next";
import { getQuestionsByCategory, getQuizById } from "../../lib/api-util";
import Quiz from "../../components/quiz/quiz";
import { ParsedUrlQuery } from "querystring";

interface QuizPageProps {
  quiz: Quiz,
  questions: Question[];
}

const QuizPage: React.FC<QuizPageProps> = ({ quiz, questions }) => {
  // we have SSR for questions so check if available
  if (!questions) {
    return <h1>LOADING QUESTIONS</h1>;
  }

  return <Quiz quiz={quiz} questions={questions}></Quiz>;
};

export default QuizPage;

// need Iparams interface for static paths
interface IParams extends ParsedUrlQuery {
  id: string;
}

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const quizId = params!.id;
  const quiz = await getQuizById(quizId);
  const questions = await getQuestionsByCategory(quiz.categories);

  return {
    props: {
      quiz,
      questions,
    },
    revalidate: 60,
  };
};

// SSR only render first quiz
export const getStaticPaths: GetStaticPaths<IParams> = async () => {
  return {
    paths: [{ params: { id: "1" } }],
    fallback: true,
  };
};
