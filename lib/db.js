import { MongoClient } from 'mongodb';

const connnectionString = `mongodb+srv://${process.env.mongodb_username}:${process.env.mongodb_password}@${process.env.mongodb_cluster}.1h9pu.mongodb.net/${process.env.mongodb_database}?retryWrites=true&w=majority`;


export async function connectDatabase() {
  const client = await MongoClient.connect(connnectionString);
  return client;
}

export async function insertDocument(client, collection, document) {
  const db = client.db();
  const result = await db.collection(collection).insertOne(document);  // javascript object insert (which is a mongodb )  
  return result;
}

export async function getAllDocuments(client, collection, sort) {
  const db = client.db();
  const documents = await db.collection(collection).find().sort(sort).toArray();
  return documents;
}


