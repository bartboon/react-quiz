import { hash, compare } from 'bcryptjs';

export async function hashPassword(password ) {
  const hashedPw = await hash(password, 12);
  return hashedPw;
}

export async function verifyPassword(password, hashedPassword) {
  const isValid  = await compare(password, hashedPassword);
  return isValid;
}