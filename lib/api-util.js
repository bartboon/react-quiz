// helper utils to get the correct events by filtering


export async function getAllQuestions() {
  const response = await fetch(
    "http://localhost:3000/api/quiz/questions"
  );
  const questionData = await response.json();
 
  return questionData;
}

export async function getQuestionsByCategory(categories) {
  const response = await fetch(
    "http://localhost:3000/api/quiz/questions"
  );
  const questionData = await response.json();
 
  const filteredByCategory = questionData.filter((question)=> categories.includes(question.category))
  return filteredByCategory;
}

export async function getAllQuizes() {
  const response = await fetch(
    "http://localhost:3000/api/quiz"
  );
  const quizData = await response.json();
  return quizData;
}

export async function getQuizById(id) {
  const allQuizes = await getAllQuizes();
  return allQuizes.find((quiz) => quiz.id === parseInt(id));
}