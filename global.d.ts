
interface Quiz {
  id: string,
  title: string,
  image: string,
  description: string,
  threshold: number,
  feedback: any,
  questions: [Question]
}

interface Question {
  id: string
  category: string,
  type: string
  title: string
  image?: string
  text : string
  answers: [ Answer]
}

interface Answer {
  text: string,
  correct: boolean
}

interface QuestionAnswer {
  question: Question,
  correct: Boolean | undefined
}
