
export enum QuizStatus {
  Success = 1,
  Error = 2,
  Incomplete = 3,
}